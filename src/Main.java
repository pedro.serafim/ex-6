import java.util.Scanner;

    public class Ex6 {

        public static void main( String[ ] args ) {

            int senhaMaster, senhaTenta;
            double n1, n2, soma;
            Scanner captura = new Scanner( System.in );

            System.out.println( "Cadastre uma senha ( Apenas Números ):");
            senhaMaster = captura.nextInt( );

            System.out.println( "Digite um número: " );
            n1 = captura.nextDouble( ) ;
            System.out.println( "Digite outro número: " );
            n2 = captura.nextDouble( );
            soma = n1 + n2;

            System.out.println( "Digite a sua senha para desbloquear:");
            senhaTenta = captura.nextInt( );

            if( senhaTenta == senhaMaster ){
                System.out.println( "A soma é: "+soma );
            }else{
                System.out.println( "Erro! Senha errada!" );
            }
        }
    }


